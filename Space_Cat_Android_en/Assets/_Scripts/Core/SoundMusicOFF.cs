﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundMusicOFF : MonoBehaviour {

    private Image m_child;

	// Use this for initialization
	void Start ()
    {
        m_child = transform.Find("Background").GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (this.GetComponent<Toggle>().isOn)
        {
            m_child.enabled = false;
        }
        else
        {
            m_child.enabled = true;
        }
	}
}

﻿using UnityEngine;
using System.Collections;

public class Back_Planet_Move : MonoBehaviour {

	private float Xmove_Speed;
	private float Xmove;
	private float Ymove;
	public bool Turn;
	public int Marzin;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 Scale = transform.localScale;

		Marzin++;

		if (Turn == false) 
		{
			Xmove_Speed = -0.01f;
			Scale.x = 0.1f;
		}
		if (Turn == true) 
		{
			Xmove_Speed = +0.01f;
			Scale.x = -0.1f;
		}

		if(Marzin > 3300)
		{
			if(Turn == false)
			{
				Turn = true;
				Marzin=0;
			}

			else if (Turn == true)
			{
				Turn = false;
				Marzin=0;
			}
		}




		Xmove = Xmove_Speed;
		transform.localScale = Scale;
		transform.Translate(Xmove, 0, 0, Space.World);
	}
}
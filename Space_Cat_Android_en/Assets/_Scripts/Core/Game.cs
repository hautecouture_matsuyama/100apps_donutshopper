﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using NendUnityPlugin.AD;

[System.Serializable]
public struct UpgradeItem
{
	public Text info;
	public Image levelIndicator;
	public Image iconImage;

	public Sprite[] UpgradeIcons;
	//public Sprite[] UpgradeIcons_zh;
}

[System.Serializable]
public struct UpgradeVisuals
{
	public Sprite[] levelIndicators;
	public UpgradeItem[] items;
}

[System.Serializable]
public struct VisualSettings 
{
	public float[] planetRadius;
	public Sprite[] planetSprites;
	//public Sprite[] planetSprites_zh;
	public GameObject[] atoms;
	public GameObject FireBall;
	public GameObject ShootingStar;

	[Space (5)]
	public UpgradeVisuals upgradeVisuals;

	[Space (5)]
	public Text MetersText;
	public Text AtomsText; 

	[Space (10)]
	public AudioClip[] atomSounds;
	public AudioClip JumpSound;
}

public class Bank
{
	private int BankBalance = 0;

	public Bank ()
	{
		// Load Data
		this.BankBalance = PlayerPrefs.GetInt ("_BankBalance", 0);    
	}

	public void Deposit (int amount)
	{
		this.BankBalance += amount;

		PlayerPrefs.SetInt ("_BankBalance", this.BankBalance);
	}

	public void Withdraw (int amount)
	{
		this.BankBalance -= amount;

		PlayerPrefs.SetInt ("_BankBalance", this.BankBalance);
	}

	public int GetBalance ()
	{
		return this.BankBalance;
	}
}

public class Game : MonoBehaviour 
{
	//private AdmobScript admobscript;

	// -- Singleton 
	// -- Singleton
	private static Game _instance;
	public static Game instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType <Game> ();
			}
			
			return _instance;
		}
	}

	private Player player;
	private PlanetManager planetManager;

	public VisualSettings visuals;

	private Planet currentPlanet;

	public bool paused { get; set; }

	// -- Current number of atoms collected
	private int AtomCount = 0;

	// -- Stores our meters travelled. 
	private int _metersTraveled;
	public int metersTraveled
	{
		get { return _metersTraveled; }
		
		set
		{
			if (value > _metersTraveled)
			{
				// -- Increased
				_metersTraveled = value;
			}
		}
	}
	
	// -- forced starting planet
	private Vector3 StartPosition {
		get {
			return new Vector3 (0, 3.5f, -0.001f);
		}
	}

	// -- UI CRAP

	public Vector2 AtomsTweenTarget
	{
		get
		{
			return Camera.main.ScreenToWorldPoint (GameObject.Find ("Atoms_Counter_Img").transform.position);
		}
	}

	public bool _IsRunning { get; set; }
	private bool tmpDead = false;

	private AchievementsManager _AchievementManager;
	private List <Achievement> _ActiveAchievements = new List <Achievement> (3);

	public Bank _Bank { get; set; }

	public const int ads = 0; // 0 = on, 1 = off

	public void Awake ()
	{
        //if (I2.Loc.LocalizationManager.CurrentLanguage == "Chinese")
        //{
			
        //}
		this._AchievementManager = new AchievementsManager ();
		this._AchievementManager.LoadAchievements ();

		 

		this.player = GameObject.Find ("_Cat_Go").GetComponent <Player> ();

		// -- Load Bank
		this._Bank = new Bank ();

		// -- Initialise tween engine
		DOTween.Init (true, true, LogBehaviour.ErrorsOnly);

		Application.targetFrameRate = 60;

		// -- are ads on ? 
		if (ads == 0)
		{
			#if UNITY_IPHONE || UNITY_ANDROID
			// Replace with your app IDs and app signatures ... 
			//Chartboost.init ("54c6cc2943150f17afbbff1b", "8ecbfd2fd8eaab405ef8e08f4c38f1e89e6618a8", "54c6cb2143150f178d293089", "df3bea981e891069e0fa34eed07ec8d9bc804eb9");
			//admobscript = new AdmobScript();
			#endif
		}
	}

	public void Start ()
	{
		this._AchievementManager.AchievementUnlocked += (sender, e) =>
		{
			UnlockAchievement (e.Data);
		};

		//if (LoadActiveAchievements () != null)
		//{
			// -- We have data, lets use it! 
			//this._ActiveAchievements = LoadActiveAchievements ();

			//SaveActiveAchievements ();
		//}
		//else
		//{
			// -- No Data, Let's create some
			//this._ActiveAchievements [0] = this._AchievementManager.RandomAchievement ();
			//this._ActiveAchievements [1] = this._AchievementManager.RandomAchievement (this._ActiveAchievements [0]);
			//this._ActiveAchievements [2] = this._AchievementManager.RandomAchievement (this._ActiveAchievements [0], this._ActiveAchievements [1]);

			//SaveActiveAchievements ();
		//}

		//UpdateGoals ();

		this.AtomCount = 0;
		this.metersTraveled = 0;

        if (Application.platform == RuntimePlatform.IPhonePlayer)
            NendAdInterstitial.Instance.Load(461156, "dd59ebee9d504f1ad7b88a8775f3e90a83265638");
        else if (Application.platform == RuntimePlatform.Android)
            NendAdInterstitial.Instance.Load(461162, "9bae1761786b7d919fa63c18f81c6892352cb307");

    }

	private int _AtomMultiplier = 1;

	public void StartGame () 
	{
		// -- Create out planet manager, handles creating the planets.
        //if (I2.Loc.LocalizationManager.CurrentLanguage == "Chinese")
        //{
        //    planetManager = new PlanetManager (this.visuals.planetSprites_zh, this.visuals.planetRadius, this.visuals.atoms);
        //}
        //else 
        //{
        //    planetManager = new PlanetManager (this.visuals.planetSprites, this.visuals.planetRadius, this.visuals.atoms);
        //}

        planetManager = new PlanetManager(this.visuals.planetSprites, this.visuals.planetRadius, this.visuals.atoms);

        ///planetManager.CleanUp ();
        this.player.GetComponent<Player>().enabled = true;
        this.player.GetComponent<Pauser>().enabled = true;
		this.player.GetComponent <Rigidbody2D> ().gravityScale = 1.0f;
		this.player.transform.position = this.StartPosition;
		this.player.Init ();
        
		this._metersTraveled = 0;
		this.visuals.MetersText.text = this.metersTraveled.ToString ("D4") + "M";

		this.AtomCount = 0;
		this.visuals.AtomsText.text = this.AtomCount.ToString ();
		
		this._IsRunning = true;
		this.paused = false;
		this.tmpDead = false;

		planetManager.Init (this.player);

		if (_AchievementManager != null)
			_AchievementManager.RegisterEvent (AchievementType.GameCount, this._ActiveAchievements.ToArray (), 1);

		this._AtomMultiplier = PlayerPrefs.GetInt ("_AtomMultiplier", 0);

		Camera.main.transform.position = new Vector3 (0, 0, -10);
        
        //this.player.GetComponent<Rigidbody2D>().isKinematic = true;
#if UNITY_IPHONE || UNITY_ANDROID
        //Chartboost.cacheInterstitial ();
        //admobscript = new AdmobScript();
#endif

    }

	void Update () 
	{
		if (planetManager == null)
			return;

		if (IsDead ())
		{
			if (!this.tmpDead)
			{
				Die ();

				this.tmpDead = true;
			}

			return;
		}

		planetManager.Update ();

		// -- only update if we're not travelling backwards
		this.metersTraveled = (Mathf.RoundToInt (StartPosition.x + player.transform.position.x));

		// -- update text
		string formattedScore = this.metersTraveled.ToString ("D4");
		this.visuals.MetersText.text = formattedScore + " m";

		DoCameraEveryFrame ();
        
    }

	public void OnPlanetLand ()
	{
		if (planetManager != null)
		{
			SetCurrentPlanet ();

			planetManager.NewPlanet ();

			//UpdateCamera ();
		}
	}

	public void Die (bool SwitchScreen = true)
	{
		this._IsRunning = false;

		this.player.transform.SetParent (null);
		this.player.GetComponent <Rigidbody2D> ().gravityScale = 0.0f;
		this.player.GetComponent <Rigidbody2D> ().velocity = Vector2.zero;
		this.player.transform.position = new Vector3 (1000, 1000, 0);

		if (planetManager != null)
			planetManager.CleanUp ();
		
		this.planetManager = null;

		if (SwitchScreen) {
            BackButtonUtil.Instance.ChangeCommand(BackKeyAction.InvokeEvent);
            BackButtonUtil.Instance.RemoveListenerAll();
            BackButtonUtil.Instance.AddListener(InterfaceManager.instance.SelectMenu);
            InterfaceManager.instance.SwitchScreen(ActiveScreen.Death);
        }
		else
			return;

		// --
		if (_AchievementManager != null)
			_AchievementManager.RegisterEvent (AchievementType.DeathCount, this._ActiveAchievements.ToArray (), 1);

		_Bank.Deposit (this.AtomCount);

		StoreBestMeters (this.metersTraveled);
		StoreBestAtoms (this.AtomCount);

		DisplayGameStats ();

		#if UNITY_IPHONE || UNITY_ANDROID
		//Chartboost.showInterstitial ();
		//admobscript = new AdmobScript();
		#endif
	}

	public void DisplayGameStats ()
	{
		Text textObj;
		string textBuffer;
		string basetext;
		// -- BEST METERS
		int _BestMeters = PlayerPrefs.GetInt ("_BestMeters"); 
		textObj = GameObject.Find ("Best_Meters_Text").GetComponent <Text> ();
		textBuffer = textObj.text;
		basetext = textBuffer.Substring(0,textBuffer.IndexOf(":")+1);
		textObj.text = basetext + " " + _BestMeters.ToString ("N0") + "m";

		// -- BEST ATOMS
		int _BestAtoms = PlayerPrefs.GetInt ("_BestAtoms"); 
		textObj = GameObject.Find ("Best_Atoms_Text").GetComponent <Text> ();
		textBuffer = textObj.text;
		basetext = textBuffer.Substring(0,textBuffer.IndexOf(":")+1);
		textObj.text = basetext + " " + _BestAtoms.ToString ("N0") + "points";
		
		// -- Current Meters
		GameObject.Find ("Final_Meters_Text").GetComponent <Text> ().text = this.metersTraveled.ToString ("N0");

		// -- Current Atoms
		GameObject.Find ("Final_Atoms_Text").GetComponent <Text> ().text = this.AtomCount.ToString ("N0");

		// -- Bank
		textObj = GameObject.Find ("Bank_Text").GetComponent <Text> ();
		textBuffer = textObj.text;
		basetext = textBuffer.Substring(0,textBuffer.IndexOf(":")+1);
		textObj.text = basetext + " " + this._Bank.GetBalance ().ToString ("N0");


        int j = UnityEngine.Random.Range(0, 51);

        Debug.Log("nend1");
        //ランダムに出た数字が25より小さかったら広告が出る
        if (j > 25)

        {
            Debug.Log("nend2");
            StartCoroutine(ShowInterStitial());


        }

    }

    private IEnumerator ShowInterStitial()
    {

        yield return new WaitForSeconds(1f);
        //NendAdInterstitial.Instance.Show();

        if (NendAdInterstitial.Instance != null)
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
                NendAdInterstitial.Instance.Show(461156);
            else if (Application.platform == RuntimePlatform.Android)
                NendAdInterstitial.Instance.Show(461159);
        }






        //AdstirInterstitialPlugin.instance.ShowAd();



    }


    void StoreBestMeters (int newMeters)
	{
		int oldMeters = PlayerPrefs.GetInt ("_BestMeters", 0);    

		if (newMeters > oldMeters)
		{
			PlayerPrefs.SetInt ("_BestMeters", newMeters);
		}
	}

	void StoreBestAtoms (int newAtoms)
	{
		int oldAtoms = PlayerPrefs.GetInt ("_BestAtoms", 0);    
		
		if (newAtoms > oldAtoms)
		{
			PlayerPrefs.SetInt ("_BestAtoms", newAtoms);
		}
	}

	/// <summary>
	/// Tweens the atom
	/// </summary>
	public void ProcessAtom (Atom _atom)
	{
		if (!_atom.collected)
		{
			AtomCount += (int)(_atom.value * UpgradeInfo.GetMultiplierPower (this._AtomMultiplier));

			if (_AchievementManager != null)
				_AchievementManager.RegisterEvent (AchievementType.CollectAtoms, this._ActiveAchievements.ToArray ());

			if (_atom.GetAtomType () == AtomType.Special)
			{
				if (_AchievementManager != null)
					_AchievementManager.RegisterEvent (AchievementType.CollectSpecialAtoms, this._ActiveAchievements.ToArray ());
			}

			// -- Set Text
			//string formattedAtomCount = this.AtomCount.ToString ("D2");
			this.visuals.AtomsText.text = this.AtomCount.ToString ();

			GetComponent<AudioSource>().PlayOneShot (this.visuals.atomSounds [UnityEngine.Random.Range (0, 3)]);

			_atom.collected = true;
		}
	}

	void UnlockAchievement (Achievement data)
	{
		for (int i = 0; i < this._ActiveAchievements.Count; i++)
		{
			if (data.Message == this._ActiveAchievements[i].Message)
			{
				Debug.Log ("Unlocked: " + data.Message);
				
				RectTransform AchievmentRect = GameObject.Find ("Achievement_Image").GetComponent <RectTransform> ();
				
				Vector2 originalPosition = AchievmentRect.position;
				Vector2 offScreenPos = new Vector2 (originalPosition.x, originalPosition.y - AchievmentRect.rect.height - 5.0f);
				
				// -- Set it off screen
				AchievmentRect.position = offScreenPos;
				
				// -- Make visible (used to hide for the first time .. );
				CanvasGroup cg = AchievmentRect.GetComponent <CanvasGroup> ();
				cg.alpha = 1.0f;
				
				// -- Set Text
				Text achievementText = AchievmentRect.Find ("_Achievement_Data_Text").GetComponent <Text> ();
				achievementText.text = data.Message + " ( " + data.reward + " )";
				
				// -- Tween this bitch
				StartCoroutine (AchievmentAnimation (AchievmentRect, originalPosition, offScreenPos));
				
				_Bank.Deposit (data.reward);
				
				// -- Save our data .. 
				this.SaveActiveAchievements ();
				this._AchievementManager.SaveAchievements ();
			}
		}
	}

	// UI EVENTS

	private void UpdateGoals ()
	{
		for (int i = 0; i < 3; i++)
		{
			Text _text = GameObject.Find ("Goal_Description_" + (i + 1)).GetComponent <Text> ();
			_text.text = this._ActiveAchievements [i].Message;
		}
	}

	private IEnumerator AchievmentAnimation (RectTransform _ARect, Vector2 to, Vector2 from)
	{
		Tween tweenTo = _ARect.DOMove (to, 0.5f);
		yield return tweenTo.WaitForCompletion ();

		// wait a second
		yield return new WaitForSeconds (1.0f);

		_ARect.DOMove (from, 0.5f);
	}
	
	public void PauseGame ()
	{
		this.paused = true;
        Pauser.Pause();
		if (GetCurrentPlanet () == null)
			return;

		GetCurrentPlanet ().PauseRotation ();
	}

	public void ResumeGame ()
	{
		StartCoroutine (Resume ());
        Pauser.Resume();
		GetCurrentPlanet ().ResumeRotation ();
	}

	private IEnumerator Resume ()
	{
		yield return new WaitForSeconds (0.15f);
		this.paused = false;
	}

	void OnDrawGizmos ()
	{
		if (!Application.isPlaying || planetManager == null)
			return;

		planetManager.Draw ();
	}

	public bool IsDead ()
	{
		Vector2 PlayersVPPos = Camera.main.WorldToViewportPoint (this.player.transform.position);

		if (PlayersVPPos.y < -0.15f)
		{
			// start animating camera ... 
			return true;
		}

		return false;
	}

	/// <summary>
	/// Sets the current planet, used for camera animation etc
	/// </summary>
	public void SetCurrentPlanet ()
	{
		this.currentPlanet = planetManager.ClosestPlanet (player.transform.position);
	}
	
	/// <summary>
	/// Tween camera to planet
	/// </summary>
	public void UpdateCamera ()
	{
		if (planetManager.NextPlanet () == null || currentPlanet == null || planetManager.GetTrajectory (currentPlanet) == null)
			return;

		Vector2[] _targets = new Vector2 [3]
		{
			currentPlanet.GetPosition (),
			planetManager.NextPlanet ().GetPosition (),
			planetManager.GetTrajectory (currentPlanet).HighestPoint ()
		};
		
		StartCoroutine (AnimateCamera (0.3f, _targets));

		// shooting star
        if (UnityEngine.Random.value > 0.3f)
		{
            Vector2 _position = Camera.main.ViewportToWorldPoint(new Vector2(UnityEngine.Random.Range(0.6f, 1.6f), UnityEngine.Random.Range(0.2f, 0.8f)));
			Instantiate (this.visuals.ShootingStar, _position, Quaternion.identity);
		}
	}

	// -- Same as 'Update Camera' but just every frame instead of on planet land ... 
	public void DoCameraEveryFrame ()
	{
		if (currentPlanet == null || planetManager.NextPlanet () == null || planetManager.GetTrajectory (currentPlanet) == null)
			return;

		Vector2[] targets = new Vector2 [3]
		{
			currentPlanet.GetPosition (),
			planetManager.NextPlanet ().GetPosition (),
			planetManager.GetTrajectory (currentPlanet).HighestPoint ()
		};


		Rect boundingBox = CameraExtensions.CalculateTargetsBoundingBox (3.5f, 6.5f, 3.0f, 3.0f, targets);
		
		Vector3 endPosition = CameraExtensions.CalculateCameraPosition (boundingBox, -10.0f); 

		Camera.main.transform.position = Vector3.Lerp (Camera.main.transform.position, endPosition, Time.deltaTime * 4.0f);
		Camera.main.orthographicSize = CameraExtensions.CalculateOrthographicSize (boundingBox, Camera.main, 5.0f, 5.0f); //Mathf.Lerp (startSize, newSize, t);
	}

	private IEnumerator AnimateCamera (float duration, Vector2[] targets)
	{
		//Rect boundingBox = CameraExtensions.CalculateTargetsBoundingBox (3.5f, 6.5f, 3.0f, 3.0f, targets);
		Rect boundingBox = CameraExtensions.CalculateTargetsBoundingBox (3.5f, 6.5f, 3.0f, 3.0f, targets);

		//Vector3 startPosition = Camera.main.transform.position;
		Vector3 endPosition = CameraExtensions.CalculateCameraPosition (boundingBox, -10.0f); 

		float t = 0.0f;

		Camera.main.transform.DOMove (endPosition, duration).SetEase (Ease.InOutQuad);

		while (t < 1) 
		{
			t += Time.smoothDeltaTime / duration;
			
			// lerp something
			//Camera.main.transform.position = Vector3.Lerp (startPosition, endPosition, t);
			Camera.main.orthographicSize = CameraExtensions.CalculateOrthographicSize (boundingBox, Camera.main, 5.0f, 5.0f); //Mathf.Lerp (startSize, newSize, t);
			
			yield return null;
		}

		yield return null;
	}

	/// <summary>
	/// Saves the active achievements.
	/// </summary>
	void SaveActiveAchievements ()
	{
		PlayerPrefs.SetString ("_ActiveAchievements", SaveUtils.ObjectToStr <List <Achievement>> (this._ActiveAchievements));
	}
	
	/// <summary>
	/// Loads the active achievements.
	/// </summary>
	private List <Achievement> LoadActiveAchievements ()
	{
		return SaveUtils.StrToObject <List <Achievement>> (PlayerPrefs.GetString ("_ActiveAchievements")); 
	}

	public Planet GetCurrentPlanet ()
	{
		return this.currentPlanet;
	}
	
	public Player GetPlayer ()
	{
		return this.player;
	}



    public Texture2D shareTexture;

    public void ShareTwitter()
    {
        string url = "";
#if UNITY_ANDROID
        url = "https://play.google.com/store/apps/details?id=com.hautecouture.DonutsHopper";
#elif UNITY_IPHONE
        url = "https://itunes.apple.com/ja/app/donutshopper/id1050857027?l=ja&ls=1&mt=8";
#endif
       // UM_ShareUtility.TwitterShare("DonutsHopper キャンディーを" + this.visuals.AtomsText.text + "p回収して、" + this.visuals.MetersText.text + " 進んだよ！奪われた自由とお菓子を取り戻そう！\n" + url + " #DonutsHopper", shareTexture);
        //Debug.Log(url);
    }


    public void ShareLINE()
    {
        string url = "";
#if UNITY_ANDROID
        url = "https://play.google.com/store/apps/details?id=com.hautecouture.DonutsHopper";
#elif UNITY_IPHONE
        url = "https://itunes.apple.com/ja/app/donutshopper/id1050857027?l=ja&ls=1&mt=8";
#endif

        string msg = "DonutsHopper キャンディーを" + this.visuals.AtomsText.text + "p回収して、" + this.visuals.MetersText.text + " 進んだよ！奪われた自由とお菓子を取り戻そう！\n" + url;
        string LINEurl = "http://line.me/R/msg/text/?" + Uri.EscapeUriString(msg);
        Application.OpenURL(LINEurl);
    }


    public void ShareFacebook()
    {
        /*
        if (!FB.IsInitialized)
        {
            FB.Init(Login, null, null);
            return;
        }
        else
        {
            Login();
        }
        */
    }

    void Login()
    {
        /*
        if (!FB.IsLoggedIn)
        {
            FB.Login("", LoginCallback);
            return;
        }
        else
        {
            LoginCallback();
        }
        */
    }

    string FeedLink = "";
    string FeedLinkName = "";
    string FeedLinkDescription = "";
    string FeedPicture = "";
    private Dictionary<string, string[]> FeedProperties = new Dictionary<string, string[]>();

    /*
    void LoginCallback(FBResult result = null)
    {
        if (FB.IsLoggedIn)
        {

#if UNITY_ANDROID
            FeedLink = "https://play.google.com/store/apps/details?id=com.hautecouture.DonutsHopper";
#elif UNITY_IPHONE
        FeedLink = "https://itunes.apple.com/ja/app/donutshopper/id1050857027?l=ja&ls=1&mt=8";
#endif

            FeedLinkName = "DonutsHopper ";
            FeedLinkDescription = "キャンディーを" + this.visuals.AtomsText.text + "p回収して、" + this.visuals.MetersText.text + " 進んだよ！奪われた自由とお菓子を取り戻そう！\n";

            FB.Feed(
                link: FeedLink,
                linkName: FeedLinkName,
                linkDescription: FeedLinkDescription,
                picture: "http://100apps.s3.amazonaws.com/000_Original/ShareImage/DonutsHopper_header.jpg", //ヘッダー画像リンク
                properties: FeedProperties
            );
        }
    }
    //Donuts Hopperキャンディーを[獲得ポイント]回収して、[距離ポイント（m）]進んだよ！奪われた自由とお菓子を取り戻そう！
    */
    
}
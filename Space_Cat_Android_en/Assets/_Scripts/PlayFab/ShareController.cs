﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SocialConnector;
using UnityEngine.UI;

public class ShareController : MonoBehaviour
{
    public void Share()
    {
        StartCoroutine(_Share());
    }

    public IEnumerator _Share()
    {
        string imgPath = Application.persistentDataPath + "/image.png";

        //前回のデータ消去
        File.Delete(imgPath);

        //スクリーンショットを投稿
        ScreenCapture.CaptureScreenshot("image.png");


        //投稿映像の保存が完了するまで待機

        while (true)
        {
            if (File.Exists(imgPath)) break;
            yield return null;
        }

        //投稿する
        string tweetText = "I have just scored " + PlayerPrefs.GetInt("CurrentScore", 0).ToString() + " in The DonutsHopper!Can you beat me ? ";
        string tweetURL;
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            tweetURL = "https://apps.apple.com/jp/app/DonutsHopper/id1050857027";
            SocialConnector.SocialConnector.Share(tweetText, tweetURL, imgPath);
        }

        else if (Application.platform == RuntimePlatform.Android)
        {
            tweetURL = "https://play.google.com/store/apps/details?id=com.hautecouture.DonutsHopper";
            SocialConnector.SocialConnector.Share(tweetText, tweetURL, imgPath);
        }
        //SocialConnector.SocialConnector.Share(tweetText, tweetURL, imgPath);


    }
}

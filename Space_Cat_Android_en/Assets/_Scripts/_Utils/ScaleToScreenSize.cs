﻿using UnityEngine;
using System.Collections;

public class ScaleToScreenSize : MonoBehaviour 
{
	public bool isOrtho, update;

	public bool animateAlpha = false;
	
	private SpriteRenderer sr;
	private float targetAlpha;
	private float speed;
	private float currentA;
	private Color lastCol;

	void Start () 
	{
		ScaleScreenInit ();

		lastCol = new Color (1.0f, 1.0f, 1.0f, 1.0f);
		
		RandomiseMe ();
		
		sr = this.GetComponent <SpriteRenderer> ();
	}

	void OnDrawGizmos ()
	{
		if (Application.isPlaying)
			return;

		ScaleScreen ();
	}

	void Update ()
	{
		if (update)
		{
			ScaleScreen ();
		}

		if (!animateAlpha)
			return;

		if (!IsApproximately (currentA, targetAlpha, 0.1f))
		{
			sr.color = Color.Lerp (lastCol, new Color (1, 1, 1, this.targetAlpha), speed * Time.smoothDeltaTime);
		}
		else
		{
			RandomiseMe ();
		}
		
		lastCol = sr.color;
		currentA = sr.color.a;
	}

	void RandomiseMe () 
	{
		targetAlpha = Random.Range (0.1f, 1.0f);
		speed = Random.Range (0.2f, 0.7f);
	}
	
	bool IsApproximately (float a, float b, float tolerance)
	{
		return Mathf.Abs (a - b) < tolerance;
	}

	void ScaleScreen () 
	{
		transform.localScale = new Vector3 (1, 1, 1);
		
		float width = GetComponent <Renderer> ().bounds.size.x;
		float height = GetComponent <Renderer> ().bounds.size.y;
		
		float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
		float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
		
		Vector3 newScale = new Vector3 (worldScreenWidth / width, 
		                                worldScreenHeight / height, 0.1f);

		transform.localScale = (newScale * 1.05f);
	}

	void ScaleScreenInit () 
	{
		transform.localScale = new Vector3 (1, 1, 1);

		float width = GetComponent <Renderer> ().bounds.size.x;
		float height = GetComponent <Renderer> ().bounds.size.y;
		
		float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
		float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
		
		Vector3 newScale = new Vector3 (worldScreenWidth / width, worldScreenHeight / height, 0.1f);

		newScale = isOrtho ? (newScale * 2) : newScale;

		transform.localScale = newScale;
	}
}
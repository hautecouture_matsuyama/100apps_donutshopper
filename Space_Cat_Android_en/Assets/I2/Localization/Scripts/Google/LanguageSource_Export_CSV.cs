﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

namespace I2.Loc
{
	public partial class LanguageSource
	{
		public string Export_CSV( string Category, char Separator = ',' )
		{
			StringBuilder Builder = new StringBuilder();
			
			int nLanguages = (mLanguages.Count);
			Builder.AppendFormat ("Key{0}Type{0}Desc", Separator);

			foreach (LanguageData langData in mLanguages)
			{
				Builder.Append (Separator);
				AppendString ( Builder, GoogleLanguages.GetCodedLanguage(langData.Name, langData.Code), Separator );
			}
			Builder.Append ("\n");
			
			foreach (TermData termData in mTerms)
			{
				string Term;

				if (string.IsNullOrEmpty(Category) || (Category==EmptyCategory && termData.Term.IndexOfAny(CategorySeparators)<0))
					Term = termData.Term;
				else
				if (termData.Term.StartsWith(Category) && Category!=termData.Term)
					Term = termData.Term.Substring(Category.Length+1);
				else
					continue;	// Term doesn't belong to this category

				//--[ Key ] --------------				
				AppendString( Builder, Term, Separator );
				
				//--[ Type and Description ] --------------
				Builder.AppendFormat (Separator + termData.TermType.ToString());
				Builder.Append (Separator);
				AppendString(Builder, termData.Description, Separator);
				
				//--[ Languages ] --------------
				for (int i=0; i<Mathf.Min (nLanguages, termData.Languages.Length); ++i)
				{
					Builder.Append (Separator);
					AppendString(Builder, termData.Languages[i], Separator);
				}
				Builder.Append ("\n");
			}
			return Builder.ToString();
		}
		
		static void AppendString( StringBuilder Builder, string Text, char Separator )
		{
			if (string.IsNullOrEmpty(Text))
				return;
			Text = Text.Replace ("\\n", "\n");
			if (Text.IndexOfAny((Separator+"\n\"").ToCharArray())>=0)
			{
				Text = Text.Replace("\"", "\"\"");
				Builder.AppendFormat("\"{0}\"", Text);
			}
			else 
				Builder.Append(Text);
		}

	}
}
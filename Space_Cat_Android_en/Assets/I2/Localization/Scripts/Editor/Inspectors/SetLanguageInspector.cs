﻿using UnityEditor;
using UnityEngine;
using System.Collections;

namespace I2.Loc
{
	[CustomEditor(typeof(SetLanguage))]
	public class SetLanguageInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			SetLanguage setLan = (SetLanguage)target;
			SerializedObject mSerializedObj = new SerializedObject( setLan );
			SerializedProperty prop = mSerializedObj.FindProperty("_Language");

			string[] Languages;
			LanguageSource source = setLan.mSource;
			if (source==null)
			{
				LocalizationManager.UpdateSources();
				Languages = LocalizationManager.GetAllLanguages().ToArray();
				System.Array.Sort(Languages);
			}
			else
			{
				Languages = source.GetLanguages().ToArray();
				System.Array.Sort(Languages);
			}

			int index = System.Array.IndexOf(Languages, prop.stringValue);

			GUI.changed = false;
			index = EditorGUILayout.Popup("Language", index, Languages);
			if (GUI.changed)
			{
				if (index<0 || index>=Languages.Length)
					prop.stringValue = string.Empty;
				else
					prop.stringValue = Languages[index];
				GUI.changed = false;
				mSerializedObj.ApplyModifiedProperties();
			}

			GUILayout.Space(5);
			if (setLan.mSource==null) GUI.contentColor = Color.Lerp (Color.gray, Color.yellow, 0.1f);
			source = EditorGUILayout.ObjectField("Language Source:", source, typeof(LanguageSource), true) as LanguageSource;
			GUI.contentColor = Color.white;

			if (GUI.changed)
				setLan.mSource = source;
		}
	}
}
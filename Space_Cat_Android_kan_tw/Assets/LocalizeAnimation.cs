﻿using UnityEngine;
using System.Collections;

public class LocalizeAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
	var anim = GetComponent<Animator>();
    if (I2.Loc.LocalizationManager.CurrentLanguage == "Chinese" || I2.Loc.LocalizationManager.CurrentLanguage == "Chinese (Taiwan)")
	{
		anim.SetInteger("LocalizationID",1);
	}
	else 
	{
		anim.SetInteger("LocalizationID",0);
	}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class SwapSprite : MonoBehaviour {

	public Sprite englishSprite;
	public Sprite chineseSprite;
	// Use this for initialization
	void Start () {
        if (I2.Loc.LocalizationManager.CurrentLanguage == "Chinese" || I2.Loc.LocalizationManager.CurrentLanguage == "Chinese (Taiwan)")
		{
			GetComponent<SpriteRenderer>().sprite = chineseSprite;
		}
		else
		{
			GetComponent<SpriteRenderer>().sprite = englishSprite;
		}
	}
	
	
}

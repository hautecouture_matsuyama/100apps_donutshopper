﻿using UnityEngine;
using System.Collections;

public class SetCurrentLanguage : MonoBehaviour {

	void Start()
	{
		if (FindObjectsOfType<SetCurrentLanguage>().Length > 1)
		{Destroy(gameObject);}
		else 
		{
		DontDestroyOnLoad(gameObject);
		GetComponent<I2.Loc.SetLanguage>().ApplyLanguage();
		}
	}
}

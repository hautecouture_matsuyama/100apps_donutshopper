﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Player : MonoBehaviour 
{
	// Rect we can't click on
	private Rect PauseRect
	{
		get {
			return new Rect (Screen.width - 100, Screen.height - 100, 100, 80);
		}
	}

	private bool OnPlanet = false;

	// -- Cached components
	private Rigidbody2D rb;
	private Animator _Animator;
	private BoxCollider2D _Collider;

	void Start () 
	{
		this.rb = this.gameObject.GetComponent <Rigidbody2D> ();
		this._Animator = this.gameObject.GetComponent <Animator> ();
		this._Collider = this.gameObject.GetComponent <BoxCollider2D> ();
	}

	public void Init ()
	{
		this.OnPlanet = false;
		this.ToggleCollider (true);
	}

	void Update () 
	{
		// -- 
		if (!Game.instance._IsRunning || Game.instance.IsDead () || PauseRect.Contains (Input.mousePosition))
		{
			return;
		}

		if (Input.GetMouseButtonDown (0) && OnPlanet && !Game.instance.paused)
		{
			if (_Animator)
                if (I2.Loc.LocalizationManager.CurrentLanguage == "Chinese" || I2.Loc.LocalizationManager.CurrentLanguage == "Chinese (Taiwan)") _Animator.Play("Squat");
				else _Animator.Play("Cat_Squat");

			if (Game.instance.GetCurrentPlanet () != null)
				Game.instance.GetCurrentPlanet ().PauseRotation ();
		}

		if (Input.GetMouseButtonUp (0) && OnPlanet && !Game.instance.paused)
		{
			if (_Animator)
                if (I2.Loc.LocalizationManager.CurrentLanguage == "Chinese" || I2.Loc.LocalizationManager.CurrentLanguage == "Chinese (Taiwan)") _Animator.Play("Jump");
				else _Animator.Play("Cat_Jump");
			
			transform.parent = null;
			rb.gravityScale = 1.0f;

			// -- 
			if (Game.instance.GetCurrentPlanet () != null)
				Game.instance.GetCurrentPlanet ().ResumeRotation ();

			Jump ();
		}

		if (OnPlanet && this.transform.parent != null)
		{
			if (!Input.GetMouseButton (0))
			{
				if (_Animator)
                    if (I2.Loc.LocalizationManager.CurrentLanguage == "Chinese" || I2.Loc.LocalizationManager.CurrentLanguage == "Chinese (Taiwan)") _Animator.Play("Breathe");
					else _Animator.Play("Cat_Breathe");
			}
			
			rb.gravityScale = 0.0f;
			rb.velocity = Vector2.zero;
			rb.angularVelocity = 0.0f;
			
			Vector3 dir = transform.position - transform.parent.position;
			
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.Euler (0f, 0f, angle - 90);
			
			Debug.DrawRay (transform.position, dir, Color.red);
		}
		else
		{
			Vector3 dir = rb.velocity;
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
			
			if (rb.velocity != Vector2.zero)
			{
				transform.rotation = Quaternion.Euler (0f, 0f, angle - 90);
			}
		}
	}


	void Jump ()
	{
		OnPlanet = false;

		Vector2 forward = transform.TransformDirection (Vector3.up) * 12.4f;

		rb.velocity = forward;

		StartCoroutine (this.CollisionFix ());
	
		Game.instance.audio.PlayOneShot (Game.instance.visuals.JumpSound);
	}

	void OnCollisionEnter2D (Collision2D collision) 
	{	
		Debug.Log ("Collision ... ");

		if (collision.transform.tag == "Fireball")
		{
			Debug.Log ("On Planet");

			ToggleCollider (false);
			Game.instance.Die ();
			return;
		}

		if (!OnPlanet)
		{
			this._Collider.enabled = false;

			Game.instance.OnPlanetLand ();

			OnPlanet = true;
			transform.parent = collision.transform;
		}
	}


	private IEnumerator CollisionFix (float duration = 0.15f)
	{
		ToggleCollider (false);
		yield return new WaitForSeconds (duration);
		ToggleCollider (true);
	}

	public void ToggleCollider (bool on)
	{
		this._Collider.enabled = on;
	}
}

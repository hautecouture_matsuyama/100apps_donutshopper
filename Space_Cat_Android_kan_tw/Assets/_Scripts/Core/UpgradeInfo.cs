﻿using UnityEngine;
using System.Collections;

public static class UpgradeInfo 
{
	// --
	// -- Magnet
	// --

	private static float[] Magnet_Powers = new float[]
	{
		0.5f, // level 0 // default ... 
		0.7f, // level 1 // 
		0.9f, // level 2 // 
		1.1f  // level 3 // 
	};

	private static int[] MagnetCosts = new int[]
	{
		2500,  // level 1
		5000,  // level 2
		10000, // level 3
		10000000
	};

	public static float GetMagnetPower (int level)
	{
		return UpgradeInfo.Magnet_Powers [level];
	}

	public static int GetMagnetCost (int level)
	{
		return UpgradeInfo.MagnetCosts [level];
	}

	// -- 
	// -- Multiplier 
	// -- 

	private static int[] Multiplier_Powers = new int[]
	{
		1, // level 0 // default ... 
		2, // level 1 // 
		3, // level 2 // 
		4  // level 3 // 
	};
	
	private static int[] MultiplierCosts = new int[]
	{
		5000,  // level 1
		10000, // level 2
		20000, // level 3
	};
	
	public static float GetMultiplierPower (int level)
	{
		return UpgradeInfo.Multiplier_Powers [level];
	}
	
	public static int GetMultiplierCost (int level)
	{
		return UpgradeInfo.MultiplierCosts [level];
	}

	// -- 
	// -- Speed 
	// -- 

	private static int[] JumpSpeedCosts = new int[]
	{
		2000,  // level 1
		6000,  // level 2
		10000, // level 3
	};

	public static int GetJumpSpeedCost (int level)
	{
		return UpgradeInfo.JumpSpeedCosts [level];
	}
}

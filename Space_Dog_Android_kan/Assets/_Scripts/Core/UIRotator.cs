﻿using UnityEngine;
using System.Collections;

public class UIRotator : MonoBehaviour 
{
	private RectTransform _RectTransform;

	public bool clockwise = false;
	public float speed = 20.0f;

	void Start () 
	{
		this._RectTransform = this.GetComponent <RectTransform> ();
	}
	
	void Update () 
	{
		float rotation = this.clockwise ? -speed * Time.deltaTime : speed * Time.deltaTime;

		this._RectTransform.Rotate (0, 0, rotation);
	}
}

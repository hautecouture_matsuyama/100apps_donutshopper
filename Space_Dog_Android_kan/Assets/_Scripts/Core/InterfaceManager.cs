﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public enum TweenDirection { Left, Right, Up, Down }
public enum ActiveScreen { Menu, Upgrades, Options, Game, Death, Goals }

[System.Serializable]
public class TweenItem 
{
	public TweenItem () {}

	public RectTransform transform;
	public TweenDirection direction = TweenDirection.Right;

	[Space (10.0f)]

	public float padding;

	[Space (10.0f)]

	public Ease ease;
	public float speed;

	public Vector2 _to
	{
		get {
			Vector2 pos = transform.position;

			switch (direction)
			{
			case TweenDirection.Left:
				pos = new Vector2 (0 - (transform.rect.width / 2) - padding, transform.position.y);
				break;

			case TweenDirection.Right:
				pos = new Vector2 (Screen.width + (transform.rect.width / 2) + padding, transform.position.y);
				break;
			
			case TweenDirection.Up:
				pos = new Vector2 (transform.position.x, Screen.height + (transform.rect.height / 2) + padding);
				break;

			case TweenDirection.Down:
				pos = new Vector2 (transform.position.x, 0 - (transform.rect.height / 2) - padding);
				break;
			}

			return pos;
		}
	}

	private Vector2 _from;

	// -- to make sure we don't start a twee twice ... 
	private bool isTweening; 

	/// <summary>
	/// Tweens to the target position
	/// </summary>
	public void TweenTo ()
	{
		if (this.isTweening)
			return;

		this.isTweening = true;

		this._from = transform.position;

		this.transform.DOMove (_to, this.speed, false).SetEase (this.ease).OnComplete (() => {
			this.isTweening = false;
		});
	}

	/// <summary>
	/// Tweens to the target position with a callback once complete.
	/// </summary>
	public void TweenTo (System.Action OnComplete)
	{
		if (this.isTweening)
			return;
		
		this.isTweening = true;
		
		this._from = transform.position;
		
		this.transform.DOMove (_to, this.speed, false).SetEase (this.ease).OnComplete (() => {
			this.isTweening = false;
			OnComplete ();
		});
	}

	/// <summary>
	/// Tweens from out target position to it's original position
	/// </summary>
	public void TweenFrom ()
	{
		if (this.isTweening)
			return;
		
		this.isTweening = true;

		this.transform.DOMove (_from, this.speed, false).SetEase (this.ease).OnComplete (() => {
			this.isTweening = false;
		});
	}

	/// <summary>
	/// Tweens from out target position to it's original position
	/// </summary>
	public void TweenFrom (System.Action OnComplete)
	{
		if (this.isTweening)
			return;
		
		this.isTweening = true;
		
		this.transform.DOMove (_from, this.speed, false).SetEase (this.ease).OnComplete (() => {
			this.isTweening = false;
			OnComplete ();
		});
	}

	// -- Set Start position outside of screen? ... then use TweenFrom to bring it back on.
	public void StartFromEnd ()
	{
		this._from = transform.position;
		this.transform.position = this._to;
	}

	public void StartFromStart ()
	{
		this._from = transform.position;
		this.transform.position = this._from;
	}

	public void DebugDraw ()
	{
		Debug.DrawLine (transform.position, _to);
	}
}

[System.Serializable]
public class TweenGroup
{
	public string name = "Tween Group";
	public bool _OnScreen;

	[Space (2.0f)]

	public TweenItem[] tweens;


	public void DebugDraw ()
	{
		for (int i = 0; i < this.tweens.Length; i++)
		{
			this.tweens [i].DebugDraw ();
		}
	}

	public void PlayAllTo ()
	{
		_OnScreen = false;

		for (int i = 0; i < this.tweens.Length; i++)
		{
			this.tweens [i].TweenTo ();
		}
	}

	public void PlayAllFrom ()
	{
		_OnScreen = true;

		for (int i = 0; i < this.tweens.Length; i++)
		{
			this.tweens [i].TweenFrom ();
		}
	}
	
	// --
	public enum _StartingPosition {
		OnScreen, OffScreen
	}

	// -- wether the GUI should start on screen or not ... 
	public void SetStartingPosition (_StartingPosition startingPosition)
	{
		for (int i = 0; i < this.tweens.Length; i++)
		{
			if (startingPosition == _StartingPosition.OffScreen)
			{
				this._OnScreen = false;

				this.tweens [i].StartFromEnd ();
			}
			else
			{
				this._OnScreen = true;

				this.tweens [i].StartFromStart ();
			}
		}
	}
}

public static class TweenUtils
{
	public static TweenGroup GetGroup (TweenGroup[] groups, string name)
	{
		for (int i = 0; i < groups.Length; i++)
		{
			if (groups [i].name == name)
			{
				return groups [i];
			}
		}

		return null;
	}
}

public class InterfaceManager : MonoBehaviour 
{
	// -- Singleton
	private static InterfaceManager _instance;
	public static InterfaceManager instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType <InterfaceManager> ();
			}
			
			return _instance;
		}
	}

	// -- Manages all tweens for each screen.
	public TweenGroup[] tweenGroups;

	// -- Our current active screen
	private ActiveScreen currentScreen = ActiveScreen.Menu;

	void Start () 
	{
		// -- 
		DOTween.Init (true, true, LogBehaviour.ErrorsOnly);

		// -- 
		for (int i = 1; i < this.tweenGroups.Length; i++)
		{
			this.tweenGroups [i].SetStartingPosition (TweenGroup._StartingPosition.OffScreen);
		}

		this.tweenGroups [0].SetStartingPosition (TweenGroup._StartingPosition.OnScreen);
	}

 	public void SelectGame ()
	{
		// fade out menu BG ... 
		SwitchScreen (ActiveScreen.Game);

		// -- 
		ToggleMenuAlpha (0.0f, 0.3f);
	
		// -- Start the game
		if (!Game.instance._IsRunning)
		{
			Game.instance.StartGame ();
		}

        AndroidBackButtonUtil.Instance.RemoveListenerAllFromOnClickBack();
        AndroidBackButtonUtil.Instance.ChangeCommand(BackKeyAction.InvokeEvent);
        AndroidBackButtonUtil.Instance.AddListenerToOnClickBack(this.ViewGoals);
        AndroidBackButtonUtil.Instance.AddListenerToOnClickBack(Game.instance.PauseGame);
	}

	public void ToggleMenuAlpha (float _alpha, float _speed)
	{
		Image MenuBG = GameObject.Find ("Menu_Background").GetComponent <Image> ();

		MenuBG.DOFade (_alpha, _speed);
	}

	public void ToggleMenuAlpha (float _alpha, float _speed, System.Action CallBack)
	{
		Image MenuBG = GameObject.Find ("Menu_Background").GetComponent <Image> ();
		
		MenuBG.DOFade (_alpha, _speed).OnComplete (() => {
			CallBack ();
		});
	}

    //メニュー呼び出し.
    //Action:QuitGame
	public void SelectMenu ()
	{
		if (Game.instance._IsRunning)
		{
			Game.instance.Die (false);
		}
        AndroidBackButtonUtil.Instance.RemoveListenerAllFromOnClickBack();
        AndroidBackButtonUtil.Instance.ChangeCommand(BackKeyAction.QuitGame);
		SwitchScreen (ActiveScreen.Menu);
	}

    //アップグレード.
    //Action: this.SelectMenu()
	public void SelectUpgrades ()
	{
        AndroidBackButtonUtil.Instance.RemoveListenerAllFromOnClickBack();
        AndroidBackButtonUtil.Instance.ChangeCommand(BackKeyAction.InvokeEvent);
        AndroidBackButtonUtil.Instance.AddListenerToOnClickBack(this.SelectMenu);

		SwitchScreen (ActiveScreen.Upgrades);

		UpdateUpgradesScreen ();
	}

    //設定.
    //Action: this.SelectMenu()
	public void SelectOptions ()
	{
        AndroidBackButtonUtil.Instance.RemoveListenerAllFromOnClickBack();
        AndroidBackButtonUtil.Instance.ChangeCommand(BackKeyAction.InvokeEvent);
        AndroidBackButtonUtil.Instance.AddListenerToOnClickBack(this.SelectMenu);

		SwitchScreen (ActiveScreen.Options);
	}

    //一時停止解除.
    //Action: this.ViewGoals()
	public void ResumeGame ()
	{
        AndroidBackButtonUtil.Instance.RemoveListenerAllFromOnClickBack();
        AndroidBackButtonUtil.Instance.ChangeCommand(BackKeyAction.InvokeEvent);
        AndroidBackButtonUtil.Instance.AddListenerToOnClickBack(this.ViewGoals);
        AndroidBackButtonUtil.Instance.AddListenerToOnClickBack(Game.instance.PauseGame);

		SwitchScreen (ActiveScreen.Game);
	}

    //一時停止.
    //Action: this.ResumeGame()
	public void ViewGoals ()
	{
        AndroidBackButtonUtil.Instance.RemoveListenerAllFromOnClickBack();
        AndroidBackButtonUtil.Instance.ChangeCommand(BackKeyAction.InvokeEvent);
        AndroidBackButtonUtil.Instance.AddListenerToOnClickBack(this.ResumeGame);
        AndroidBackButtonUtil.Instance.AddListenerToOnClickBack(Game.instance.ResumeGame);

		SwitchScreen (ActiveScreen.Goals);
	}

	private void UpdateUpgradesScreen ()
	{
		GameObject.Find ("Upgrades_Screen").transform.FindChild ("AtomBankText").GetComponent <Text> ().text = Game.instance._Bank.GetBalance ().ToString ("N0");

		int speedLevel = PlayerPrefs.GetInt ("_JumpLevel", 0);
		Game.instance.visuals.upgradeVisuals.items [0].levelIndicator.sprite = GetIndicatorSprite (speedLevel);
		Game.instance.visuals.upgradeVisuals.items [0].info.text = (speedLevel <= 2) ? UpgradeInfo.GetJumpSpeedCost (speedLevel).ToString ("N0") : "MAX";
		Game.instance.visuals.upgradeVisuals.items [0].iconImage.sprite = Game.instance.visuals.upgradeVisuals.items [0].UpgradeIcons [speedLevel];

		int multiplierLevel = PlayerPrefs.GetInt ("_AtomMultiplier", 0);
		Game.instance.visuals.upgradeVisuals.items [1].levelIndicator.sprite = GetIndicatorSprite (multiplierLevel);
		Game.instance.visuals.upgradeVisuals.items [1].info.text = (multiplierLevel <= 2) ? UpgradeInfo.GetMultiplierCost (multiplierLevel).ToString ("N0") : "MAX";
		Game.instance.visuals.upgradeVisuals.items [1].iconImage.sprite = Game.instance.visuals.upgradeVisuals.items [1].UpgradeIcons [multiplierLevel];

		int magnetLevel = PlayerPrefs.GetInt ("_MagnetLevel", 0);
		Game.instance.visuals.upgradeVisuals.items [2].levelIndicator.sprite = GetIndicatorSprite (magnetLevel);
		Game.instance.visuals.upgradeVisuals.items [2].info.text = (magnetLevel <= 2) ? UpgradeInfo.GetMagnetCost (magnetLevel).ToString ("N0") : "MAX";
		Game.instance.visuals.upgradeVisuals.items [2].iconImage.sprite = Game.instance.visuals.upgradeVisuals.items [2].UpgradeIcons [magnetLevel];
	}

	private Sprite GetIndicatorSprite (int level)
	{
		return Game.instance.visuals.upgradeVisuals.levelIndicators [level];
	}

	// -- upgrades ... 
	public void UpgradeJump ()
	{
		int currentLevel = PlayerPrefs.GetInt ("_JumpLevel", 0);
		
		if (currentLevel >= 3)
			return;
		
		// -- if we can afford it
		if (Game.instance._Bank.GetBalance () > UpgradeInfo.GetJumpSpeedCost (currentLevel))
		{
			Game.instance._Bank.Withdraw (UpgradeInfo.GetJumpSpeedCost (currentLevel));
			
			if (currentLevel < 3)
				currentLevel += 1;
			
			PlayerPrefs.SetInt ("_JumpLevel", currentLevel);
		}
		
		this.UpdateUpgradesScreen ();
	}

	public void UpgradeMagnet ()
	{
		int currentLevel = PlayerPrefs.GetInt ("_MagnetLevel", 0);

		if (currentLevel >= 3)
			return;

		// -- if we can afford it
		if (Game.instance._Bank.GetBalance () >= UpgradeInfo.GetMagnetCost (currentLevel))
		{
			Game.instance._Bank.Withdraw (UpgradeInfo.GetMagnetCost (currentLevel));

			if (currentLevel < 3)
				currentLevel += 1;

			PlayerPrefs.SetInt ("_MagnetLevel", currentLevel);
		}

		this.UpdateUpgradesScreen ();
	}

	public void ToggleMusic ()
	{
		bool mute = Camera.main.audio.volume > 0.0f ? true : false;

		if (mute)
		{
			Camera.main.audio.Pause ();
		}
		else
		{
			Camera.main.audio.Play ();
		}

		Camera.main.audio.volume = mute ? 0.0f : 1.0f;
	}

	public void ToggleSound ()
	{
		float volume = Game.instance.audio.volume > 0.0f ? 0.0f : 0.2f;
		Game.instance.audio.volume = volume;
	}

	public void UpgradeMultiplier ()
	{
		int currentLevel = PlayerPrefs.GetInt ("_AtomMultiplier", 0);

		if (currentLevel >= 3)
			return;
		
		// -- if we can afford it
		if (Game.instance._Bank.GetBalance () > UpgradeInfo.GetMultiplierCost (currentLevel))
		{
			Game.instance._Bank.Withdraw (UpgradeInfo.GetMagnetCost (currentLevel));

			if (currentLevel < 3)
				currentLevel += 1;

			PlayerPrefs.SetInt ("_AtomMultiplier", currentLevel);
		}
		
		this.UpdateUpgradesScreen ();
	}

#if UNITY_EDITOR
	void OnDrawGizmos ()
	{
		for (int i = 0; i < this.tweenGroups.Length; i++)
		{
			this.tweenGroups [i].DebugDraw ();
		}
	}
#endif

	public void SwitchScreen (ActiveScreen _screen)
	{
		// -- Make our current screen tween out 
		TweenGroup _OldGroup = TweenUtils.GetGroup (this.tweenGroups, currentScreen.ToString ());
		_OldGroup.PlayAllTo ();  

		// -- 
		this.currentScreen = _screen;

		// -- 
		TweenGroup _NewGroup = TweenUtils.GetGroup (this.tweenGroups, currentScreen.ToString ());
		_NewGroup.PlayAllFrom (); 
	}
}

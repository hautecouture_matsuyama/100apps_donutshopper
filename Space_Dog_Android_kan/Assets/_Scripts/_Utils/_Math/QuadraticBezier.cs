﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 3 point Quadratic bezier spline.
/// </summary>
public class QuadraticBezier
{
	private int pointCount;
	public Vector3[] points = new Vector3 [3];

	public QuadraticBezier (int _pointCount, Vector3 start, Vector3 middle, Vector3 end)
	{
		this.pointCount = _pointCount;
	
		this.points [0] = start;
		this.points [1] = middle;
		this.points [2] = end;
	}

	public Vector3[] GetSplinePoints ()
	{
		Vector3[] points = new Vector3 [this.pointCount];

		Vector3 p0 = this.points [0];
		Vector3 p1 = this.points [1];
		Vector3 p2 = this.points [2];

		float t; 
		Vector3 p;

		for (int i = 0; i < this.pointCount; i++) 
		{
			t = i / (this.pointCount - 1.0f);
			p = (1.0f - t) * (1.0f - t) * p0 + 2.0f * (1.0f - t) * t * p1 + t * t * p2;

			points [i] = p;
		}

		return points;
	}
}

﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public enum BackKeyAction
{
    QuitGame,
    SceneChange,
    InvokeEvent,
}

public class AndroidBackButtonUtil : MonoBehaviour {

    [SerializeField]
    BackKeyAction backKeyAction;

    //SceneChange
    [SerializeField]
    string nextSceneName;

    //DoMethod
    [SerializeField]
    UnityEvent onClickBack;

    // ----- Instance -----
    static AndroidBackButtonUtil instance;
    public static AndroidBackButtonUtil Instance
    {
        get {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(AndroidBackButtonUtil)) as AndroidBackButtonUtil;
                if (instance == null)
                {
                    Debug.Log("BackButtonUtil is nothing");
                }
            }
            return instance;
        }
    }

    // ----- Method -----
    void Update()
    {
        GetBackKey();
    }

    void GetBackKey()
    {
        //if (Application.platform != RuntimePlatform.Android) return;
        if (!Input.GetKeyDown(KeyCode.Escape)) return;

        switch (backKeyAction)
        {
            case BackKeyAction.QuitGame:
                Application.Quit();
#if UNITY_EDITOR
                Debug.Break();
#endif
                break;
            case BackKeyAction.SceneChange:
                Application.LoadLevel(nextSceneName);
                break;
            case BackKeyAction.InvokeEvent:
                onClickBack.Invoke();
                break;
        }
    }

    public void ChangeCommand(BackKeyAction action) {
        backKeyAction = action;
    }

    public void AddListenerToOnClickBack(UnityAction action)
    {
        onClickBack.AddListener(action);
    }

    public void RemoveListenerFromOnClickBack(UnityAction action)
    {
        onClickBack.RemoveListener(action);
    }

    public void RemoveListenerAllFromOnClickBack()
    {
        onClickBack.RemoveAllListeners();
    }
}
